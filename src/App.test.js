import { render, screen } from '@testing-library/react';
import App from './App';

test('renders dictionary card', () => {
  render(<App />);
  const linkElement = screen.getByText(/dictionary card/i);
  expect(linkElement).toBeInTheDocument();
});
