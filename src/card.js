import { useEffect, useState } from 'react';
import words from './words'
var index = process.env.REACT_APP_INDEX || 0
const IndexPosition = parseInt(index) 
const CycleInterval = 5000
function MyButton({ title, index, onClick }) {
    return (
        <button onClick={onClick}>
            [{index}]{title}
        </button>
    );
}

function Card() {
    const [index, setIndex] = useState(IndexPosition);
    const [card, setCard] = useState({ base: {}, target: {} })
    const [site] = useState({ text: 'http://dictionary.cambridge.org/dictionary/english-turkish/', image: `https://www.bing.com/images/search?q=` })

    function frameOpen(link, frameName) {
        window.open(link, frameName)
    }

    function proccess(index) {
        setIndex(index);
        let base = words.en[index.toString()]
        if (base) {
            let target = words.tr[index.toString()]
            base.index = index
            target.index = index
            target.link = `${site.text}${base.word}`
            target.image = `${site.image}${base.word}`
            let card = {
                base, target
            }
            console.log("init", card)
            setCard(card)
            frameOpen(target.link, "textFrame")
            frameOpen(target.image, "imageFrame")
        }
    }

    function beforeItem() {
        if (index !== 1) {
            proccess(index - 1)
        }
    }

    function nextItem() {
        proccess(index + 1)
    }

    useEffect(() => {
        const timer = setInterval(() => {
            nextItem();
        }, CycleInterval);
        // clearing interval
        return () => clearInterval(timer);
    });

    return (

        <div>
            <h5>{card.base.index}</h5>
            <h1>
                <a name="link" href={card.target.link} target="textFrame">
                    [{card.base.part}] {card.base.word}
                </a>

            </h1>

            <h1 >{card.target.word} </h1>

            <MyButton title="Back" index={index - 1} onClick={() => beforeItem()} />
            <MyButton title="Next" index={index + 1} onClick={() => nextItem()} />


        </div>

    );
}

export default Card;
