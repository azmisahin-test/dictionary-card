import './App.css';
import Card from './card';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>dictionary card</h1>
        <Card></Card>
      </header>
    </div>
  );
}

export default App;
